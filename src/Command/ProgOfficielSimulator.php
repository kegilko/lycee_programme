<?php


namespace Console\Command;

use Console\ProgrammesOfficiels;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class ProgOfficielSimulator extends Command
{
    protected static $defaultName = 'lycee:programmes-officiels:simulator';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription("pour réviser ce qu'il y a dans le programme officiel (raccourcis l:p:s)")

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Un jeux video comme tu n'en as jamais vu!")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $scores = [];
        $helper = $this->getHelper('question');

        $output->getFormatter()->setStyle('title', new OutputFormatterStyle('white', null, ['bold']));

        $output->writeln("<title>\n  ---------------------------------------- </>");
        $output->writeln("<title>< SUPER PROGRAMMES DE LYCEE SIMULATOR 3000 ></>");
        $output->writeln("<title>  ---------------------------------------- \n\n</>");



        $PO = new ProgrammesOfficiels();
        $matieres = $PO->getMatieres();

        $question = (new ChoiceQuestion(
            "<fg=blue>Choisir un ensemble de matière(s) (all)</>",
            array_merge(["-1" => "all"], $matieres),
            0
        ))->setMultiselect(true);

        $matieresChosen = array_unique($helper->ask($input, $output, $question));

        if (in_array("all", $matieresChosen)) $matieresChosen = $matieres;

        $question = (new ChoiceQuestion(
            "\n<fg=blue>Choisir un ensemble de type de question(s) (all)</>",
            array_merge(["-1" => "all"], $PO->getTypeQuestions()),
            0
        ))->setMultiselect(true);


        $typeQuestionsChosen = array_unique($helper->ask($input, $output, $question));
        if (in_array("all", $typeQuestionsChosen)) $typeQuestionsChosen = $PO->getTypeQuestions();
        $PO->setTypeQuestionsChosen($typeQuestionsChosen);

        $PO->removeMatiere(array_diff($matieres, $matieresChosen));

        $PO->buildQuestionnaire();

        $questions = $PO->getQuestions();
        $nbr_question = count($questions);


        $session = 1;
        while(1){
            shuffle($questions);
            $rep_juste = 0;
            $current_info_score = "";

            foreach ($questions as $key => $question){

                system('clear');
                if($current_info_score == "" && empty($scores)) $output->write("\n<options=bold>Un total de $nbr_question questions a été généré !</>");
                if($key > 0) {
                    $output->write("\n<fg=magenta;options=bold>$current_info_score</>");
                }else{
                    $output->writeln("\n\n<options=bold>> Depart de la session num $session ! <</>");
                }
                if(!empty($scores)) $output->writeln("\n ( " . implode(' ',$scores) . " ) " );

                $num_question = $key + 1 ;
                $output->writeln("\n<fg=blue>Question ${num_question}/${nbr_question} : <fg=blue;options=bold>" . $question[0] . "</></><fg=white> [entrer] </>");
                $helper->ask($input, $output, new Question(""));
                $output->writeln("<fg=blue>Reponse : </><fg=cyan;options=bold>" . PHP_EOL . " - " . implode(PHP_EOL . ' - ',$question[1]) . '</>');

                $question = new ChoiceQuestion(
                    "\n <fg=blue>Reponse juste ? (oui)</>",
                    ['oui', 'non'],
                    0
                );
                $valid = $helper->ask($input, $output, $question);
                if ($valid == "oui") $rep_juste++;

                $percent = round($rep_juste / $num_question * 100, 2);
                $current_info_score = "[$session : $rep_juste/$num_question ($percent%)]";
            }
            array_push($scores, "<fg=magenta>$current_info_score</>");

            $session++;
        }


        //var_dump($PO->getQuestions());
    }
}