## Installation

### Pré-requis

- php version >= 7.1.3
- composer >= 1.9.0

##### Installer php 

- `apt install php7.2`

##### Installer composer (gestionnaire de package php)

- les instructions directement sur [le site officiel] 
- une fois le composer.phar en local, le renommer composer et l'envoyer (par exemple) dans /usr/bin. Mieux encore, dans ~/bin en ajoutant ~/bin dans le $PATH   

## Déployer l'application

- ouvrir un terminal à la racine du projet
- installer les packages php via la commande `composer install`

## Démarrer le jeu 

- ouvrir un terminal à la racine du projet
- exécuter la commande `./bin/run l:p:s`

# Enjoy !

[le site officiel]: https://getcomposer.org/download/
